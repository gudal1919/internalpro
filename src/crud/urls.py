

"""crud URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url,include
from django.contrib import admin
from crudproject import views
from django.conf import settings
from django.conf.urls.static import static
import os

urlpatterns = [
	url(r'^$', views.list, name='list'),
    url(r'^login/$', views.login, name='login'),
    url(r'^reupdate/$', views.user, name='reupdate'),
    # url(r'^profile/(?P<id>[0-9]+)/$', views.profile, name='profile'),
    
    url(r'^update/$', views.updated, name='updated'),
    url(r'^update/(?P<id>[0-9]+)/$', views.update, name='update'),
    url(r'^delete/(?P<id>[0-9]+)/$', views.delete, name='delete'),
    url(r'^admin/', admin.site.urls),

    url(r'^api-auth/', include('rest_framework.urls'))
    
    
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)




"""
urlpatterns = [
    url(r'^$', views.list, name='list'),
    url(r'^admin/', admin.site.urls),
    # url(r'^img/$', views.img, name='img'),
    # url(r'^newimg/$', views.newimg, name='newimg'),
    url(r'^reupdate/$', views.user, name='user'),
    url(r'^updated/(?P<id>[0-9]+)/$', views.updated, name='updated'),
    url(r'^delete/(?P<id>[0-9]+)/$', views.delete, name='delete'),
    url(r'^update/(?P<id>[0-9]+)/$', views.update, name='update'),
    url(r'^update/$', views.newsave, name='new-save'),
    url(r'^login/$', views.login, name='login'),
    url(r'^api/', include('rest_framework.urls')),
    # url(r'^localhost/$', views.local, name='local'),
    # url(r'^localcount/$', views.localcount, name='localcount'),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


"""