# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-23 09:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crudproject', '0002_auto_20180923_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='updated',
            name='active',
            field=models.BooleanField(default=True),
        ),
    ]
