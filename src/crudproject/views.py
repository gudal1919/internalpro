from django.http import Http404
from django.shortcuts import get_object_or_404, render,redirect
from django.utils import timezone
from django import forms
from .models import Updated

from django.conf import settings




def list(request):
    context={}
    return render(request, 'list.html', context)

# def img(request):
#     context={}
#     return render(request, 'imagespage.html', context)

# def newimg(request):
#     context={}
#     return render(request, 'newimg.html', context)

def user(request):
    context={}
    return render(request, 'reupdate.html', context)

def updated(request):
    context={}
    if request.method=="POST":
        k=request.POST.__getitem__('fullname')
        k1=request.POST.__getitem__('address')
        k2=request.POST.__getitem__('email')
        k3=request.POST.__getitem__('contact')
        k4=request.POST.__getitem__('password')
        s=Updated(fullname=k,address=k1,email=k2,contact=k3,password=k4) 
        s.save()

        updates=Updated.objects.filter(active=True)
        context["updates"]=updates
    else:
        updates=Updated.objects.filter(active=True)
        context["updates"]=updates

    return render(request, 'updated.html', context)

def update(request,id):
    id=int(id)
    user = Updated.objects.get(pk=id)
    context={'user':user}
    return render(request, 'userupdate.html', context)

def delete(request,id):
    context={}
    id=int(id)
    user = Updated.objects.get(pk=id)
    user.active=False
    user.save()
    return redirect('/update/')

def newsave(request):
    # return redirect('/updated/')
    id =request.POST.__getitem__('userId')
    id=int(id)

    user = Updated.objects.get(pk=id)
    j=request.POST.__getitem__('fullname')
    j1=request.POST.__getitem__('address')
    j2=request.POST.__getitem__('email')
    j3=request.POST.__getitem__('contact')
    j4=request.POST.__getitem__('password')
    # Updated.objects.all().update(fullname=j,address=j1,email=j2,contact=j3,password=j4)
    Updated.objects.filter(pk=id).update(fullname=j,address=j1,email=j2,contact=j3,password=j4)
    print("updated all")
    return redirect('/updated/')

def login(request):
    context={}
    return render(request, 'login.html', context)

def local(request):
    context={}
    return render(request, 'localhost.html', context)    

def localcount(request):
    context={}
    return render(request, 'localcount.html', context)

