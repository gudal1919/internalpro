from django.db import models

class Updated(models.Model):
	fullname = models.CharField(max_length=200)
	address =  models.CharField(max_length=200)
	email = models.CharField(max_length=200)
	contact =  models.IntegerField()
	password=models.CharField(max_length=20,default='')
	active =  models.BooleanField(default=True)
	def __str__(self):
		return self.fullname+" - "+self.address+" - "+str(self.id)