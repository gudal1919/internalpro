from django.apps import AppConfig


class CrudprojectConfig(AppConfig):
    name = 'crudproject'
